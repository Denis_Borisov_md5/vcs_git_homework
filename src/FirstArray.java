public class FirstArray
{
    public static int monthhours()
    {
        int hours = 24;
        return hours;
    }
    public static void main (String[] args)
    {
        int month_days[] = new int[12]; //объявляем массив

        //month_days = new int[12]; //выделяем память под массив
        //можно int month_days[] = new int [12]; - объявление + инициализация
        //присваивание значений элементам массива
        month_days[0] = 31;
        month_days[1] = 28;
        month_days[2] = 31;
        month_days[3] = 30;
        month_days[4] = 31;
        month_days[5] = 30;
        month_days[6] = 31;
        month_days[7] = 31;
        month_days[8] = 30;
        month_days[9] = 31;
        month_days[10] = 30;
        month_days[11] = 31;
        //можно int month_days[] = {31, 28, 31, 30 ... 31};
        //вывод значения 4го элемента массива
        System.out.println("In april there are " + month_days[3] + " days.");
        System.out.println("In april there are " + month_days[3]*monthhours() + " hours.");

        double result = 0;
        for (int i = 0; i < 12; i++)
            result = result + month_days[i];
        System.out.println("All days in year div by 12: " + result/12);
    }
}
